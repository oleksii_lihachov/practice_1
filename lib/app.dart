import 'package:flutter/material.dart';
import 'dart:math';

import './phrase.dart';
import './textControl.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AppState();
  }
}

class _AppState extends State<App> {
  final _random = new Random();
  final _phrases = const ['One', 'Two', 'Three', 'Four', 'Five', 'Six'];
  var _phraseIndex = 0;

  void _handleChange() {
    setState(() {
      _phraseIndex = _random.nextInt(5);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(40),
      child: Center(
        child: Column(
          children: [
            Phrase(_phrases[_phraseIndex]),
            TextControl(_handleChange),
          ],
        ),
      ),
    );
  }
}
