import 'package:flutter/material.dart';

class Phrase extends StatelessWidget {
  final String copy;

  Phrase(this.copy);

  @override
  Widget build(BuildContext context) {
    return Text(
      copy,
      textAlign: TextAlign.center,
    );
  }
}
