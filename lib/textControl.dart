import 'package:flutter/material.dart';

class TextControl extends StatelessWidget {
  final Function handleClick;

  TextControl(this.handleClick);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text('Change text'),
      onPressed: handleClick,
    );
  }
}
