import 'package:flutter/material.dart';

import './app.dart';

void main() => runApp(_MyApp());

class _MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Practice 1'),
        ),
        body: App(),
      ),
    );
  }
}
